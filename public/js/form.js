function clearResponse() {
  $('form').find('input').removeClass('input-error')
  $('ul.errors').empty();
  $('ul.errors').hide();

  $('span.success').hide();
}

function setErrors(errors) {
  $('ul.errors').show();

  for(field in errors) {
    for(index in errors[field]) {
      $('form').find('input[name=' + field + ']').addClass('input-error');
      $('ul.errors').append('<li>' + errors[field][index] + '</li>');
    }
  }
}

function setSuccess(data) {
  $('span.success').show();
  $('span.success').find('a').attr('href', data.url);
  $('span.success').find('a').text(data.url);
}

function setSpinner(status) {
  if(status) {
    $('form').find('button[type=primary] i.fa').show();
  }
  else {
    $('form').find('button[type=primary] i.fa').hide();
  }
}

$('form').submit(function(e) {
  clearResponse();
  setSpinner(true);

  $.ajax({
    url: '/',
    type: 'POST',
    data: $(this).serialize(),
    success: function(data) {
      clearResponse();
      setSuccess(data);
    },
    error: function(xhr, status, error) {
      setErrors(JSON.parse(xhr.responseText).errors);
    },
    complete: function() {
      setSpinner(false);
    }
  })

  e.preventDefault();
});

$(document).ready(function(){
  setSpinner(false);
  clearResponse();
});
