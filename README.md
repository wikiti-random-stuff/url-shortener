# URL shortener

## Summary

A simple URL shortener using [this](https://gitlab.com/wikiti-random-stuff/sinatra-template) sinatra template.

Live demo is right [here](http://dhc-url-shortener.herokuapp.com).

## Installation

Clone this repo, install the dependencies with

````sh
bundle install
````

And execute the server with

````sh
bundle exec rackup
````

Links may be accessed using the following format: `http://<app-host>/l/<name>`.

Note that this app can forward query parameters. For example, if you shorten `http://www.google.com` to `google`, and make a request to `http://<app-host>/l/google?q=3+5` then it will redirect to `http://www.google.com?q=3+5`. It also redirect all HTTP actions (`GET`, `POST`, ...).

## Authors

This project has been developed by:

| Avatar | Name | Nickname | Email |
| ------- | ------------- | --------- | ------------------ |
| ![](http://www.gravatar.com/avatar/2ae6d81e0605177ba9e17b19f54e6b6c.jpg?s=64)  | Daniel Herzog | Wikiti | [wikiti.doghound@gmail.com](mailto:wikiti.doghound@gmail.com) |
