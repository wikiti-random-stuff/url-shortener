get '/css/global.css' do
  less :global
end

helpers do
  def protected!
    return if authorized?
    headers['WWW-Authenticate'] = 'Basic realm="Restricted Area"'
    halt 401, "Not authorized\n"
  end

  def authorized?
    return true if credentials.empty?

    @auth ||=  Rack::Auth::Basic::Request.new(request.env)
    @auth.provided? && @auth.basic? && @auth.credentials && @auth.credentials == credentials
  end

  def credentials
    @credentials ||= [ ENV['ADMIN_USER'], ENV['ADMIN_PASSWORD'] ].compact
  end
end
