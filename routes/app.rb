get '/' do
  slim :index
end

post '/' do
  content_type :json

  url = URL.create name: params[:name], url: params[:url]
  return { url: url.path(request.host) }.to_json if url.errors.empty?

  halt 400, { errors: url.errors.to_hash }.to_json
end

get '/urls' do
  @urls = URL.all
  slim :urls
end

route :get, :post, :delete, :patch, :put, :head, :options, '/l/:name' do
  obj = URL.first(name: params[:name])
  halt 404 if obj.nil?

  redirect "#{obj.url}?#{request.query_string}"
end
