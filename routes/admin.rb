get '/admin' do
  protected!
  @urls = URL.all

  slim :admin
end

get '/admin/:id/delete' do
  protected!
  URL.get(params[:id]).destroy

  redirect '/admin'
end
