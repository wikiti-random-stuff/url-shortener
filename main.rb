libdir = File.dirname(__FILE__)
$LOAD_PATH.unshift(libdir) unless $LOAD_PATH.include?(libdir)

DEPENDENCIES = %w(sinatra sinatra/contrib/all data_mapper less tilt/less slim json)
DEV_DEPENDENCIES = %w(sinatra/reloader pry)

# Dependencies
DEPENDENCIES.each { |d| require d }
DEV_DEPENDENCIES.each { |dd| require dd } if development?

# Load DB items
DataMapper::setup(:default, ENV['DATABASE_URL'] || "sqlite3://#{Dir.pwd}/database.db")
Dir[libdir + '/models/*.rb'].each { |file| require file }
DataMapper.finalize
DataMapper.auto_upgrade!

# Load routes
Dir[libdir + '/routes/*.rb'].each { |file| require file }

# Setup assets
Less.paths << settings.views
