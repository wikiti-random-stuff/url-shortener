class URL
  include DataMapper::Resource

  RESERVED = %w{}
  REGEX = /^[0-9A-Za-z_\-]+$/

  property :id, Serial
  property :name, String, unique: true, required: true
  property :url, Text, format: :url, required: true

  validates_with_method :name, method: :check_valid_name

  def check_valid_name
    return [false, "Name cannot be one of the following: #{RESERVED.join ', '}"] if RESERVED.include?(name)
    return [false, "Name must contain only alphanumeric characters, underscores or hyphens"] unless name =~ /^[0-9A-Za-z_\-]+$/

    true
  end

  def path(host = 'test')
    "http://#{host}/l/#{name}"
  end
end
